import Head from 'next/head'
import Layout, { siteTitle } from '../components/Layout'
import utilStyles from '../styles/utils.module.css'

export default function Home() {
  return (
    <Layout title="Home| Next.js" 
    description="esta es la descripción del home" home>
        <section className={utilStyles.headingMd}>
        <p>Sitio Hecho con Next.js</p>
      </section>
    </Layout>
  )
}